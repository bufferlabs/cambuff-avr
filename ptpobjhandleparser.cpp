#include "ptpobjhandleparser.h"

void
PTPObjHandleParser::Parse (const uint16_t len, const uint8_t * pbuf,
			   const uint32_t & offset)
{
  address =
    ((uint32_t) pbuf[len - 5] << 24) + ((uint32_t) pbuf[len - 6] << 16) +
    ((uint32_t) pbuf[len - 7] << 8) + ((uint32_t) pbuf[len - 8]);
}

uint32_t
PTPObjHandleParser::GetLastAddress (void)
{
  return address;
}

void
PTPRawParser::Parse (const uint16_t len, const uint8_t * pbuf,
		     const uint32_t & offset)
{
  Serial.write (pbuf, len / 2);
  delay (10);
  Serial.write (pbuf + len / 2, len / 2);
}
